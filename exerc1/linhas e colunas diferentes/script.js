var linhaA = prompt('MATRIZ A\nNúmero de linhas:')
var colunaA = prompt('MATRIZ A\nNúmero de colunas:')
var linhaB = prompt('MATRIZ B\nNúmero de linhas:')
var colunaB = prompt('MATRIZ B\nNúmero de colunas:')
if (colunaA == linhaB) {
    //CRIAÇÃO DA MATRIZ A PELO USUÁRIO
    var matrizA = []
    console.log('MATRIZ A')  //No console
    for (var linha = 1; linha <= linhaA; linha++) {
        var aux = []
        for (var coluna = 1; coluna <= colunaA; coluna++) {
            aux.push(prompt('MATRIZ A\nElemento [' + linha + ', ' + coluna + ']:')) 
            console.log('Elemento [' + linha + ', ' + coluna + ']: ' + aux[aux.length - 1])  //No console
        }
        matrizA.push(aux)
    }
    console.log(matrizA)
    //CRIAÇÃO DA MATRIZ B PELO USUÁRIO
    var matrizB = []
    console.log('MATRIZ B')  //No console
    for (var linha = 1; linha <= linhaB; linha++) {
        var aux = []
        for (var coluna = 1; coluna <= colunaB; coluna++) {
            aux.push(prompt('MATRIZ B\nElemento [' + linha + ', ' + coluna + ']:'))
            console.log('Elemento [' + linha + ', ' + coluna + ']: ' + aux[aux.length - 1])  //No console
        }
        matrizB.push(aux)
    }
    console.log(matrizB)  
    //MULTIPLICAÇÃO DAS MATRIZES
    var matrizC = []
    console.log('PRODUTO DAS MATRIZES:')  //No console
    for (var linha = 0; linha < linhaA; linha++) {
        var linhaC = []
        for (var vezes = 0; vezes < colunaB; vezes++) {
            var soma = 0
            for (var coluna = 0; coluna < linhaB; coluna++) {
                soma = soma + Number(matrizA[linha][coluna] * matrizB[coluna][vezes])
            }
            linhaC.push(soma)
        }
        matrizC.push(linhaC)
    }
    console.log(matrizC)
}
else {
    alert('ERRO!\nO número de colunas da Matriz A é diferente do número de linhas da Matriz B.')
}


